package net.clockworkcode.config;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Optional;

import static java.nio.file.StandardOpenOption.CREATE;
import static java.nio.file.StandardOpenOption.TRUNCATE_EXISTING;

/**
 * Created by david on 1/03/2016.
 */
public class LastRun {
    private Path lastDir;

    public LastRun(Path configDir) {
        lastDir = Paths.get(configDir.toString(), "last");
        if (!Files.exists(lastDir)) {
            try {
                Files.createDirectories(lastDir);
            } catch (IOException e) {
                throw new RuntimeException("Could not create the runtime folder " + lastDir.toString());
            }
        }

    }

    public Optional<LocalDateTime> getLast(String key) {
        final Path lastFile = genLastFile(key);
        System.out.println("last file = " + lastFile.toString());
        LocalDateTime last = null;
        if (Files.exists(lastFile)) {
            try {
                String lastContent = new String(Files.readAllBytes(lastFile));
                last = LocalDateTime.parse(lastContent);
            } catch (IOException e) {
                throw new RuntimeException("could not read the content of last file " + lastFile.toAbsolutePath().toString());
            }
        }
        return Optional.ofNullable(last);
    }

    public void setLast(String key, LocalDateTime last) {
        String lastDate = last.toString();
        Path lastFile = genLastFile(key);
        try {
            Files.write(lastFile, lastDate.getBytes(), CREATE, TRUNCATE_EXISTING);
        } catch (IOException e) {
            throw new RuntimeException("Could not write last file " + lastFile.toAbsolutePath().toString());
        }
    }

    /**
     * Set the last time for the given key, the ZonedDateTime is converted to a local date time for supplied zoneId
     * @param key - store the localDateTime for this key
     * @param last - can be any time zone, will be converted to the supplied zone
     * @param zoneId - convert to a local time in this zone
     */
    public void setLast(String key, ZonedDateTime last, ZoneId zoneId) {
        setLast(key, last.withZoneSameInstant(ZoneId.systemDefault()).toLocalDateTime());
    }

    private Path genLastFile(String key) {
        return Paths.get(lastDir.toString(), key.toLowerCase() + ".txt");
    }

    public void clear(String abc) {
        Path lastFile = genLastFile(abc);
        try {
            Files.deleteIfExists(lastFile);
        } catch (IOException e) {
            throw new RuntimeException("Issue deleting file " + lastFile.toAbsolutePath().toString());
        }
    }
}

