package net.clockworkcode.config;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Optional;

import static org.junit.Assert.assertEquals;

/**
 * Created by david on 2/03/2016.
 */
public class LastRunTest {

    private final String KEY1 = TestUtil.randomLengthAlphaNumeric(1,10);
    private LocalDateTime now = LocalDateTime.now();
    private LocalDateTime later = now.plusMinutes(5);

    private LastRun lastRun = new LastRun(Paths.get("out"));

    @Before
    public void before() {
        lastRun.clear(KEY1);
    }
    @After
    public void after() {
        lastRun.clear(KEY1);
    }

    @Test
    public void testLast() throws Exception {
        assertEquals(Optional.empty(), lastRun.getLast(KEY1));
        setAndCheck(now, KEY1);
        setAndCheck(later, KEY1);
    }
    @Test
    public void testLastZone() throws Exception {
        assertEquals(Optional.empty(), lastRun.getLast(KEY1));
        lastRun.setLast(KEY1, ZonedDateTime.of(2014, 1, 1, 12, 0, 0, 0, ZoneOffset.UTC.normalized()), ZoneId.systemDefault());
        assertEquals(LocalDateTime.of(2014, 1, 1, 22, 0, 0, 0), lastRun.getLast(KEY1).get());
        setAndCheck(later, KEY1);
    }

    private void setAndCheck(LocalDateTime now, String key) {
        lastRun.setLast(key, now);
        assertEquals(now, lastRun.getLast(KEY1).get());
    }
}